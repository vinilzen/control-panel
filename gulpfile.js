'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var Mincer = require('mincer');
var mince = require("gulp-mincer");

var env = new Mincer.Environment();
env.appendPath("./");
env.appendPath("./js");
env.appendPath("./node_modules/bootstrap-sass/assets/javascripts/");


// Static server
gulp.task('server', ['sass', 'pug', 'js'], function() {
    browserSync.init({
        reloadDelay: 500,
        notify: false,
        open: false,
        server: {
            baseDir: "./"
        },
        ghostMode: false
    });
    
    gulp.watch("./*.pug", ['pug']);
    gulp.watch("./scss/*.scss", ['sass']);
    gulp.watch("./index.html", reload);
    gulp.watch("./main.js", ['js']);
});

gulp.task('js', function() {
    gulp.src("./main.js")
        .pipe( mince(env) )
        .pipe( gulp.dest("./js") );
});

gulp.task('pug', function() {
  return gulp.src('./index.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('./'));
});

gulp.task('sass', function() {
    return gulp.src('./scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: ['./node_modules/bootstrap-sass/assets/stylesheets']
        }).on('error', sass.logError))
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest('./css'))
        .pipe(browserSync.stream());
});

gulp.task('default', ['server']);